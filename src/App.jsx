import React, { useState, useEffect } from "react";
//material-ui components
import { Box, Container, Grid } from "@mui/material";
import Filters from "components/Filters";
//libs
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
//redux
import { getAll, liberar, remove } from "redux/data/todo";
//components
import Header from "components/Header";
import TodoCard from "components/TodoCard";
import BackdropLoad from "components/BackdropLoad";
import NewTodo from "components/NewTodo";
import CssBaseline from "@mui/material/CssBaseline";
import Footer from "components/Footer";
import BgColor from "components/BgColor";
import DeleteToDo from "components/DeleteToDo";

const App = () => {
  const dispatch = useDispatch();
  const { data, isLoading } = useSelector((state) => state.todo);
  const { bgColor } = useSelector((state) => state.app);
  const [selectedTodo, setSelectedTodo] = useState([]);
  const [deleteTodo, setDeleteTodo] = useState({
    showDialog: false,
    todo: null,
  });
  const filters = [
    {
      value: 0,
      label: "Fecha creación (desc)",
      key: "createAt",
      order: "desc",
    },
    {
      value: 1,
      label: "Fecha venc (asc)",
      key: "expirationDate",
      order: "asc",
    },
    {
      value: 2,
      label: "Estado Tarjeta",
      key: "estatus",
      order: "asc",
    },
  ];
  const [currentFilter, setCurrentFilter] = useState(filters[0]);

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const handleSelected = (todo) => {
    if (selectedTodo.includes(todo)) {
      return setSelectedTodo((prevState) => {
        return prevState.filter((item) => item !== todo);
      });
    }
    setSelectedTodo((prevState) => {
      return [...prevState, todo];
    });
  };
  const liberarTodos = async () => {
    dispatch(liberar(selectedTodo)).then(() => setSelectedTodo([]));
  };
  const handleFilter = (event) => {
    setCurrentFilter(filters[event.target.value]);
  };
  const handleDelete = async () => {
    setDeleteTodo((prev) => ({
      showDialog: !prev.showDialog,
      todo: null,
    }));
    dispatch(remove(deleteTodo.todo));
  };
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
        backgroundImage: "url(assets/todo.png)",
        backgroundColor: bgColor,
        backgroundSize: "cover",
      }}
    >
      <CssBaseline />
      <BgColor />
      <DeleteToDo
        open={deleteTodo.showDialog}
        setOpen={() =>
          setDeleteTodo((prev) => ({
            showDialog: !prev.showDialog,
            todo: null,
          }))
        }
        handleDelete={handleDelete}
      />
      {isLoading && <BackdropLoad />}
      <Container component="main" sx={{ mt: 4, mb: 2 }} maxWidth="md">
        <Grid
          container
          spacing={2}
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid item xs={12}>
            <Header />
          </Grid>
          <Grid item xs={12}>
            <NewTodo />
          </Grid>
          {data.length > 0 && (
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Filters
                    selectedTodo={selectedTodo}
                    liberarTodos={liberarTodos}
                    currentFilter={currentFilter}
                    filters={filters}
                    handleFilter={handleFilter}
                  />
                </Grid>
                {_.orderBy(
                  data,
                  [currentFilter.key],
                  [currentFilter.order]
                ).map((todo) => (
                  <Grid item xs={12} sm={6} key={todo.id}>
                    <TodoCard
                      handleSelected={handleSelected}
                      isSelected={selectedTodo.some(({ id }) => id === todo.id)}
                      todo={todo}
                      showDeleteDialog={() =>
                        setDeleteTodo((prev) => ({
                          showDialog: !prev.showDialog,
                          todo,
                        }))
                      }
                    />
                  </Grid>
                ))}
              </Grid>
            </Grid>
          )}
        </Grid>
      </Container>
      <Footer />
    </Box>
  );
};
export default App;
