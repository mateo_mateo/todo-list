import React from "react";
//material-ui components
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
//libs
import { useDispatch } from "react-redux";
//redux
import { changeBgColor } from "redux/data/app";

const BgColor = () => {
  const dispatch = useDispatch();
  const colors = [
    "#37474f",
    "#66bb6a",
    "#29b6f6",
    "#ffa726",
    "#f44336",
    "#ce93d8",
    "#424242",
  ];
  return (
    <Box
      sx={(t) => ({
        position: "fixed",
        display: "flex",
        [t.breakpoints.down("md")]: {
          display: "none",
        },
        top: 10,
        right: 10,
      })}
    >
      {colors.map((color) => (
        <Paper
          key={color}
          onClick={() => dispatch(changeBgColor(color))}
          sx={(t) => ({
            cursor: "pointer",
            m: 0.5,
            borderRadius: "50%",
            border: `2px solid white`,
            padding: t.spacing(1.5),
            backgroundColor: color,
            "&:hover": {
              transform: "scale(1.1)",
            },
          })}
        />
      ))}
    </Box>
  );
};
export default BgColor;
