import React from "react";
//material-ui components
import TextField from "@mui/material/TextField";
import FilterIcon from "@mui/icons-material/FilterAltOutlined";
import InputAdornment from "@mui/material/InputAdornment";

const OrderBy = ({ currentFilter, filters, handleFilter }) => {
  return (
    <TextField
      select
      size="small"
      label="Order by"
      value={currentFilter.value}
      onChange={handleFilter}
      SelectProps={{
        native: true,
      }}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <FilterIcon />
          </InputAdornment>
        ),
      }}
    >
      {filters.map((option) => (
        <option key={option.label} value={option.value}>
          {option.label}
        </option>
      ))}
    </TextField>
  );
};
export default OrderBy;
