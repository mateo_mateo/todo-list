import React from "react";
//material-ui components
import { Box, Button } from "@mui/material";
import OrderBy from "./OrderBy";
import CheckIcon from "@mui/icons-material/CheckCircleOutlineOutlined";

const Filters = ({
  liberarTodos,
  selectedTodo,
  currentFilter,
  filters,
  handleFilter,
}) => {
  return (
    <Box
      sx={(t) => ({
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "white",
        p: 2,
        borderRadius: 2,
        [t.breakpoints.down("sm")]: {
          flexDirection: "column",
        },
      })}
    >
      <Button
        variant="contained"
        color="success"
        startIcon={<CheckIcon />}
        disabled={!selectedTodo.length}
        onClick={liberarTodos}
        sx={{
          textTransform: "none",
        }}
      >
        Mark as done
      </Button>
      <OrderBy
        filters={filters}
        currentFilter={currentFilter}
        handleFilter={handleFilter}
      />
    </Box>
  );
};
export default Filters;
