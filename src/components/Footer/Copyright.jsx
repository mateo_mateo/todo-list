import React from "react";
//material-ui components
import Typography from "@mui/material/Typography";

const Copyright = () => {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {`Copyright © Carlos Díaz ${new Date().getFullYear()}.`}
    </Typography>
  );
};
export default Copyright;
