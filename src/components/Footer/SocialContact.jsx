import React from "react";
//material-ui components
import Box from "@mui/material/Box";

const SocialContact = () => {
  return (
    <Box
      sx={{
        justifyContent: "center",
        display: "flex",
      }}
    >
      <a
        href="https://www.linkedin.com/in/carlos-d%C3%ADaz-palacios-0a777111a/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img
          style={{ margin: "0px 8px" }}
          width="24"
          height="24"
          src="assets/linkedin.png"
          alt="Linkedin"
        />
      </a>
      <a
        href="https://gitlab.com/cdiaz-dev"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img
          style={{ margin: "0px 8px" }}
          width="24"
          height="24"
          src="assets/gitlab.png"
          alt="Gitlab"
        />
      </a>
    </Box>
  );
};
export default SocialContact;
