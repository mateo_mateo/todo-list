import React from "react";
//material-ui components
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
//components
import Copyright from "./Copyright";
import SocialContact from "./SocialContact";

const Footer = () => {
  return (
    <Box
      component="footer"
      sx={{
        py: 2,
        px: 2,
        mt: "auto",
        backgroundColor: (theme) => theme.palette.grey[100],
      }}
    >
      <Container maxWidth="md">
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Copyright />
          <SocialContact />
        </Box>
      </Container>
    </Box>
  );
};
export default Footer;
