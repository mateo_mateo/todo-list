//material-ui components
import {
  Avatar,
  Card,
  CardContent,
  Checkbox,
  Box,
  Grid,
  LinearProgress,
  TextField,
  Typography,
  IconButton,
  Tooltip,
} from "@mui/material";
import CheckIcon from "@mui/icons-material/CheckOutlined";
import ErrorIcon from "@mui/icons-material/CloseOutlined";
import TimeIcon from "@mui/icons-material/AccessTimeOutlined";
import DeleteIcon from "@mui/icons-material/DeleteForeverOutlined";
//libs
import { useDispatch } from "react-redux";
//redux
import { update } from "redux/data/todo";

const TodoCard = ({
  todo,
  isSelected,
  handleSelected,
  showDeleteDialog,
  ...props
}) => {
  const dispatch = useDispatch();
  const handleChange = (newDate) => {
    if (todo.expirationDate !== newDate) {
      dispatch(
        update({
          ...todo,
          expirationDate: newDate,
        })
      );
    }
  };
  const TODO_STATUS = {
    realizada: { icon: <CheckIcon />, color: "success" },
    pendiente: { icon: <TimeIcon />, color: "warning" },
    atrasada: { icon: <ErrorIcon />, color: "error" },
  };
  return (
    <Card
      sx={{
        height: "100%",
        borderRadius: 4,
        "&:hover": {
          transform: "scale(1.01)",
        },
      }}
      {...props}
    >
      <LinearProgress
        color={TODO_STATUS[todo.estatus].color}
        variant="determinate"
        value={100}
        sx={{ height: 6 }}
      />
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={10}>
            <Typography color="textSecondary" variant="body2">
              {todo.details}
            </Typography>
          </Grid>
          <Grid item xs={2} align="right">
            <Tooltip title="Delete ToDo">
              <IconButton onClick={showDeleteDialog}>
                <DeleteIcon sx={{ "&:hover": { color: "red" } }} />
              </IconButton>
            </Tooltip>
          </Grid>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Checkbox
                disabled={todo.estatus === "realizada"}
                checked={isSelected}
                onChange={() => handleSelected(todo)}
              />
              <Box
                flexGrow={1}
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  alignItems: "center",
                }}
              >
                <TextField
                  size="small"
                  type="date"
                  label="Expiration"
                  value={todo.expirationDate}
                  onChange={({ target: { value } }) => handleChange(value)}
                  sx={{
                    mr: 2,
                  }}
                />
                <Avatar
                  sx={(theme) => ({
                    bgcolor:
                      theme.palette[TODO_STATUS[todo.estatus].color].main,
                    height: 40,
                    width: 40,
                    [theme.breakpoints.down("sm")]: {
                      height: 32,
                      width: 32,
                    },
                  })}
                >
                  {TODO_STATUS[todo.estatus].icon}
                </Avatar>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
export default TodoCard;
