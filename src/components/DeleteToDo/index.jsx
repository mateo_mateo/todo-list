import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

const DeleteToDo = ({ open, setOpen, handleDelete }) => {
  return (
    <Dialog open={open} onClose={setOpen} fullWidth>
      <DialogTitle>Do you want to delete this ToDo?</DialogTitle>
      <DialogContent>
        <DialogContentText>
          If you delete this ToDo you will not be able to recover it, are you
          sure you want to delete it?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={setOpen} sx={{ textTransform: "none" }}>
          Cancel
        </Button>
        <Button
          onClick={handleDelete}
          sx={{ textTransform: "none" }}
          variant="contained"
          color="error"
        >
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default DeleteToDo;
