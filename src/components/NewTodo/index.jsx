import React, { useState } from "react";
//material-ui components
import {
  Fab,
  Grid,
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Alert,
  Link,
  Tooltip,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
//redux
import { create } from "redux/data/todo";
//libs
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

const NewTodo = () => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  //Initial state
  const { data } = useSelector((state) => state.todo);
  const [todo, setTodo] = useState({
    details: "",
    expirationDate: "",
  });
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setTodo((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleSubmit = async () => {
    const newTodo = {
      ...todo,
      createAt: new Date().getTime(),
      estatus: getStatus(todo.expirationDate),
    };
    setOpen((prevState) => !prevState);
    dispatch(create(newTodo)).then(() => {
      setTodo({
        details: "",
        expirationDate: "",
      });
    });
  };
  const getStatus = (expirationDate) => {
    const currentFecha = moment().format("YYYY-MM-DD");
    if (expirationDate < currentFecha) return "atrasada";
    return "pendiente";
  };
  return (
    <div>
      {data.length > 0 ? (
        <Tooltip title="Crear Tarea">
          <Fab
            color="primary"
            onClick={handleClickOpen}
            sx={(t) => ({
              position: "fixed",
              bottom: 40,
              right: 40,
              transform: "scale(1.2)",
              [t.breakpoints.down("sm")]: {
                transform: "scale(1.1)",
                bottom: 70,
                right: 20,
              },
              zIndex: 9999,
            })}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
      ) : (
        <Alert severity="info">
          No hay cosas por hacer, puedes crear una{" "}
          <span component="button" variant="body2" onClick={handleClickOpen}>
            <Link
              sx={{
                cursor: "pointer",
              }}
            >
              aquí
            </Link>
          </span>
        </Alert>
      )}
      <Dialog open={open} onClose={handleClose} fullWidth>
        <DialogTitle>New ToDo</DialogTitle>
        <DialogContent dividers>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                label="Details"
                name="details"
                value={todo.details}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Expiration"
                name="expirationDate"
                type="date"
                value={todo.expirationDate}
                onChange={handleChange}
                InputLabelProps={{ shrink: true }}
                fullWidth
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions
          sx={{
            padding: 3,
          }}
        >
          <Button onClick={handleClose}>Cancel</Button>
          <Button
            disabled={!todo.details || !todo.expirationDate}
            onClick={handleSubmit}
            variant="contained"
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
export default NewTodo;
