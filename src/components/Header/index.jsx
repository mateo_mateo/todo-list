import React from "react";
//material-ui components
import Box from "@mui/material/Box";
//components
import CurrentDate from "./CurrentDate";
import Title from "./Title";

const Header = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 2,
      }}
    >
      <Title />
      <CurrentDate />
    </Box>
  );
};
export default Header;
