import React from "react";
//material-ui components
import { Typography } from "@mui/material";
//libs
import moment from "moment";

const CurrentDate = () => {
  return (
    <Typography variant="h6" color="white">
      {moment().format("DD/MM/YYYY")}
    </Typography>
  );
};
export default CurrentDate;
