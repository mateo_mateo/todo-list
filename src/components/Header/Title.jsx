import React from "react";
//material-ui components
import Typography from "@mui/material/Typography";

const Title = () => {
  return (
    <Typography variant="h4" color="white">
      THINGS TO DO
    </Typography>
  );
};
export default Title;
