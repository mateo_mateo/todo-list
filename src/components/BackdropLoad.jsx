//material-ui components
import { Backdrop, CircularProgress } from "@mui/material";

const BackdropLoad = () => {
  return (
    <Backdrop
      sx={{
        color: "#fff",
        zIndex: (theme) => theme.zIndex.drawer + 1,
      }}
      open={true}
    >
      <CircularProgress />
    </Backdrop>
  );
};
export default BackdropLoad;
