//libs
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import moment from "moment";

const fakeAsync = async () => {
  await new Promise((resolve) => setTimeout(resolve, 500));
};
export const getAll = createAsyncThunk(
  "todo/getAll",
  async (_, { rejected }) => {
    try {
      await fakeAsync();
      return;
    } catch (e) {
      return rejected();
    }
  }
);
export const create = createAsyncThunk(
  "todo/create",
  async (todo, { rejected }) => {
    try {
      await fakeAsync();
      return { id: new Date().getTime(), ...todo };
    } catch (e) {
      return rejected();
    }
  }
);
export const update = createAsyncThunk(
  "todo/update",
  async (todo, { rejected }) => {
    try {
      const currentFecha = moment().format("YYYY-MM-DD");
      const estatus =
        todo.expirationDate < currentFecha ? "atrasada" : "pendiente";
      await fakeAsync();
      return { ...todo, estatus };
    } catch (e) {
      return rejected();
    }
  }
);
export const liberar = createAsyncThunk(
  "todo/liberar",
  async (todos, { rejected }) => {
    try {
      await fakeAsync();
      return todos;
    } catch (e) {
      return rejected();
    }
  }
);
export const remove = createAsyncThunk(
  "todo/remove",
  async (todo, { rejected }) => {
    try {
      await fakeAsync();
      return todo;
    } catch (e) {
      return rejected();
    }
  }
);
const todoSlice = createSlice({
  name: "todo",
  initialState: {
    data: [],
    isLoading: false,
  },
  extraReducers: {
    [getAll.pending]: (state) => {
      state.isLoading = true;
    },
    [getAll.fulfilled]: (state) => {
      state.data = state.data.sort((a, b) => b.id - a.id);
      state.isLoading = false;
    },
    [getAll.rejected]: (state) => {
      state.isLoading = false;
    },
    [create.pending]: (state) => {
      state.isLoading = true;
    },
    [create.fulfilled]: (state, { payload }) => {
      state.data = [payload, ...state.data];
      state.isLoading = false;
    },
    [create.rejected]: (state) => {
      state.isLoading = false;
    },
    [update.pending]: (state) => {
      state.isLoading = true;
    },
    [update.fulfilled]: (state, { payload }) => {
      state.data = state.data.map((item) =>
        item.id === payload.id ? (item = payload) : item
      );
      state.isLoading = false;
    },
    [update.rejected]: (state) => {
      state.isLoading = false;
    },
    [liberar.pending]: (state) => {
      state.isLoading = true;
    },
    [liberar.fulfilled]: (state, { payload }) => {
      state.data = state.data.map((item) =>
        payload.some((todo) => todo.id === item.id)
          ? { ...item, estatus: "realizada" }
          : item
      );
      state.isLoading = false;
    },
    [liberar.rejected]: (state) => {
      state.isLoading = false;
    },

    [remove.pending]: (state) => {
      state.isLoading = true;
    },
    [remove.fulfilled]: (state, { payload }) => {
      state.data = state.data.filter((item) => item.id !== payload.id);
      state.isLoading = false;
    },
    [remove.rejected]: (state) => {
      state.isLoading = false;
    },
  },
});
export default todoSlice.reducer;
