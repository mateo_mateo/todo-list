//libs
import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
  name: "app",
  initialState: {
    bgColor: "#37474f",
  },
  reducers: {
    changeBgColor: (state, { payload }) => {
      state.bgColor = payload;
    },
  },
  extraReducers: {},
});
export const { changeBgColor } = appSlice.actions;
export default appSlice.reducer;
