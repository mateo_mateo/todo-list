import { combineReducers } from "@reduxjs/toolkit";
import app from "../data/app";
import todo from "../data/todo";

export const reducer = combineReducers({
  todo,
  app,
});
